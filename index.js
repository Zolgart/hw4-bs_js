
/*
 Теоретичні питання
 1. Цикл в програмуванні - це конструкція, повторно виконує
    певну кількість разів або доти, доки виконується певна умова.

 2. У JavaScript є кілька видів циклів: for, while, do while

 3. Основна відмінність між циклами do while і while полягає в тому, що цикл do while 
    завжди виконується один раз, навіть якщо умова не виконується.




 Практичні завдання
*/

// Завдання 1
let num1, num2;


do {
    num1 = prompt("Введіть перше число:");
} while (isNaN(num1)); 

do {
    num2 = prompt("Введіть друге число:");
} while (isNaN(num2)); 

num1 = parseInt(num1);
num2 = parseInt(num2);


for (let i = Math.min(num1, num2); i <= Math.max(num1, num2); i++) {
    console.log(i);
}


let userNumber;


do {
    userNumber = prompt("Введіть число:");
} while (isNaN(userNumber) || userNumber % 2 !== 0); 

console.log("Введене число є парним:", userNumber);
